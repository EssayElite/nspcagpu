//
// Created by user on 2-11-16.
//

#include "gtest/gtest.h"

#include "../cxxsrc/util/random.h"
#include "../cxxsrc/util/CMatrix.h"
#include "../cxxsrc/util/montecarlo.hpp"
#include <Eigen/Dense>
using namespace Eigen;
#include "../cxxsrc/util/io_helper.h"
#include "../cxxsrc/util/GMatrix.h"
#include "../cudasrc/GView.cuh"
#include "../cxxsrc/util/DataTransformer.h"
#include "../cudasrc/GPUAlgorithm.h"
template<typename T1, typename T2, typename T>
bool assertNear(const T1 & lhs, const T2 & rhs, const T threshold){
    if (lhs.rows()!=rhs.rows()){
        return false;
    }
    if (lhs.cols()!=rhs.cols()){
        return false;
    }

    for (int i=0;i<lhs.rows();i++){
        for (int j=0;j<lhs.cols();j++){
            T lhs_data = lhs(i,j);
            T rhs_data = rhs(i,j);
            if (abs(lhs_data-rhs_data)>threshold){
                return false;
            }
        }
    }
    return true;
};

TEST(TESTforOPERATOR, DISABLED_TESTForOPERATOR){
    CMatrixCM<double> mat(10, 10);
    mat(0,0) = 1.0;

    for (int i=0;i<10;i++){
        for (int j =0; j<10;j++){
            mat(i,j)=1.0;
        }
    }

    std::cout<< mat << std::endl;
    double result= mat(0,0);
    ASSERT_EQ(result,1.0);
}
TEST(TEST_LIB, DISABLED_TEST_FOR_FILLER){
//    forced_init();
    ASSERT_NEAR(1,1,0.1);
}


TEST(TEST_LIB, OVERLOADING_OS_STREAM){
    CMatrixCM<double> mat(10,10);
    mat.setConstant(1.0);
    std::cout << mat << std::endl;
}



TEST(MONTECARLO, GENERATE_MONTECARLO_SAMPLE){
    MonteCarlo<MatrixXd, MatrixXi> monte_carlo(10, 5, 2);
    MatrixXi data(10, 5);
    monte_carlo.generate(data, 5, 0.2, -0.2);
    std::cout << "True A is " << std::endl << monte_carlo.trueA << std::endl;
    std::cout << "True P is " << std::endl << monte_carlo.trueP << std::endl;

    std::cout << "Epsilon's are " << std::endl << monte_carlo.epsilon << std::endl;
    std::cout << "Generated Data is " << std::endl << data << std::endl;
    ASSERT_TRUE(true);

}

TEST(TEST_IO, INPUT){
    bool result = test_input();
    ASSERT_TRUE(result);
}

TEST(TEST_DATA_TRANSFORMER, TRANSFORMDATA){
    bool result = test_copy_from_cpu_and_back();
    ASSERT_TRUE(result);
}

TEST(TEST_LINK, DISABLED_TEST){
    double * ptr = (double*)cudaMalloc((void**)&ptr, sizeof(double));
    cudaStream_t  stream;
    cudaStreamCreate(&stream);
    GPUAlgorithm<double, 4, 4>::Frobenius(ptr, 100, 10, stream);
    ASSERT_TRUE(true);
}