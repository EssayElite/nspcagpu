//
// Created by user on 10-11-16.
//


#include <gtest/gtest.h>
#include "../cudasrc/GPUAlgorithm.h"
#include "../cxxsrc/util/GMatrix.h"

using namespace std;
class CudaTest : public ::testing::Test{
public:
    const int row = 10;
    const int col = 10;
    GMatrix<double> dev_mat;
    cudaStream_t  stream;
    CudaTest(): dev_mat(10,10){
        cudaStreamCreate(&stream);
    }

    ~CudaTest(){
        cudaStreamDestroy(stream);
    }

};

TEST_F(CudaTest, SetConstant){
    GPUAlgorithm<double, 4, 4>::setConstant(dev_mat.data(),dev_mat.rows()*dev_mat.cols(),2.0, 4, stream);
    dev_mat.print(stream);
    cudaStreamSynchronize(stream);
    ASSERT_TRUE(true);
}