//
// Created by user on 2-11-16.
//

#include "gtest/gtest.h"
#include "../cxxsrc/NSPCAsolver.h"
#include <iostream>
#include "../cxxsrc/util/MatHelper.hpp"
using std::cout; using std::endl;
#include "../cxxsrc/util/montecarlo.hpp"
#include <Eigen/Dense>
using namespace Eigen;

using std::cout;
using std::endl;
class NSPCAtest: public ::testing::Test{
public:
    const int N =20;
    const int P =5;
    const int p= 2;

    const int numThreads= 4;
    MonteCarlo<MatrixXd, MatrixXi> monteCarlo;
    NSPCAsolver solver;
    MatrixXi data;
    MatrixXi restriction;
    NSPCAtest():monteCarlo(N, P, p), solver(N, P, p, 1.5, numThreads){
        data=MatrixXi::Constant(N, P, 0);
        restriction=MatrixXi::Constant(p, P, 2);
        monteCarlo.generate(data, 1, 0.3, -0.3);
//    cout << "Generated data is  " << data << endl;
        solver.initialize_solver(data, restriction, 0.0, 2.0, 20);
    }
};

TEST_F(NSPCAtest, test_initialization){
    cout << "A after initialization is " << endl << solver.matA <<endl;
    cout << "P after initialization is " << endl << solver.matP <<endl;
    cout << "Z after initialization is " << endl << solver.matZ <<endl;

    double mean = MatHelper::average(solver.matZ.col(0));
    double variance = MatHelper::variance(solver.matZ.col(0));

    cout << "Mean of first column is " << endl << mean << endl;
    cout << "variance of first column is " << endl << variance << endl;
    ASSERT_TRUE(true);
}

TEST_F(NSPCAtest, test_first_blas_operation){

    bool result = solver.solve_A_test_blas_operation();
    ASSERT_TRUE(result);
}

TEST_F(NSPCAtest, test_for_svd){
    bool result = solver.solve_A_test_svd();
    ASSERT_TRUE(result);
}

TEST_F(NSPCAtest, test_for_solve_a){
    solver.solve_A();
    ASSERT_TRUE(true);
}