//
// Created by user on 2-11-16.
//

#include <gtest/gtest.h>
#include "../cxxsrc/util/CMatrix.h"
#include "../cxxsrc/util/random.h"
#include "magma.h"
#include "../cxxsrc/util/GMatrix.h"
#include <Eigen/Dense>
#include "../cxxsrc/util/DataTransformer.h"
#include "../cxxsrc/util/MagmaWrapper.h"
#include "../cudasrc/GPUAlgorithm.h"

class MagmaTest :public ::testing::Test{
public:
    Random<double> random;
    double * data1;
    double * data2;
    magma_ptr  gpu_data;
    const size_t nRow = 10;
    const size_t nCol = 10;
    GMatrix<double> matrix;
    magma_queue_t magma_queue;
    MagmaTest():matrix(10,10){
        magma_init();
        magma_int_t dev = 0;
        magma_queue_create_v2(dev, &magma_queue);
        magma_stream = magma_queue_get_cuda_stream(magma_queue);
        magma_dmalloc_pinned(&data2, nRow*nCol);
        magma_malloc(&gpu_data, sizeof(double)*nRow*nCol);
    }
    ~MagmaTest(){
        magma_free_pinned(data2);
        magma_free(gpu_data);
        magma_finalize();
    }

    cudaStream_t magma_stream;
};

TEST_F(MagmaTest, Initialization){
    ASSERT_EQ(1,1);
}

TEST_F(MagmaTest, TestPrint){
    cudaStream_t stream;
    cudaStreamCreate(&stream);
    GMatrix<double> gpu_mat(10,10);
    Eigen::MatrixXd cpu_mat = Eigen::MatrixXd::Constant(10,10, 0.2);
    DataTransformer<double, Eigen::MatrixXd>::copyCPUtoGPU(cpu_mat, gpu_mat, stream);
    gpu_mat.print(stream);
    ASSERT_TRUE(true);
}

TEST_F(MagmaTest, matmulTN){
    using namespace Eigen;
    MatrixXd lhs = MatrixXd::Constant(5,10, 0.2);
    MatrixXd rhs = MatrixXd::Constant(5, 7, 0.5);
    MatrixXd result = lhs.transpose()* rhs;
    cout << "CPU results are  " << endl << result << endl;
    GMatrix<double> dev_lhs(lhs, magma_stream);
    GMatrix<double> dev_rhs(rhs, magma_stream);
    cout << "Rhs is " << endl;
    dev_rhs.print(magma_stream);
    GMatrix<double> dev_result(result, magma_stream);
    GPUAlgorithm<double, 4, 4>::setConstant(dev_result.data(), dev_result.rows()*dev_result.cols(), 4, 0.0, magma_stream);
//    cudaStreamSynchronize(magma_stream);
    MagmaWrapper::matmulTN(dev_result, dev_lhs, dev_rhs, 1.0, 0.0, magma_queue);
    magma_queue_sync(magma_queue);

    cout << "Results in devices are " << endl;
    dev_result.print(magma_stream);
    cudaStreamSynchronize(magma_stream);
    ASSERT_TRUE(true);
}