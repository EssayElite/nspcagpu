//
// Created by user on 7-11-16.
//

#ifndef NSPCA_MACRO_H
#define NSPCA_MACRO_H
#include <cstdlib>
#include <cstdio>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#ifdef __CUDACC__
#define CUDA_DEVICE __device__
#else
#define CUDA_DEVICE
#endif

#ifdef __CUDACC__
#define CUDA_KERNEL __global__
#else
#define CUDA_KERNEL
#endif

#ifdef __CUDACC__
#define CUDA_HOST __host__
#else
#define CUDA_HOST
#endif

#define __IR__(i,j, nRow,nCol) ((i)*(nCol)+(j))
#define __IC__(i,j, nRow,nCol) ((i)+(j)*(nRow))

#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))

static void HandleError( cudaError_t err,
                         const char *file,
                         int line ) {
    if (err != cudaSuccess) {
        printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
                file, line );
        exit( EXIT_FAILURE );
    }
}
#endif //NSPCA_MACRO_H
