//
// Created by user on 8-11-16.
//

#ifndef NSPCA_GPUALGORITHM_H
#define NSPCA_GPUALGORITHM_H
#include "macro.h"
template<typename ScalarType, unsigned int numThreads, unsigned int numThreadsForReduction>
class GPUAlgorithm {
public:
    static ScalarType Frobenius(const ScalarType * input1_devptr, const size_t size, int numBlocks, cudaStream_t& stream);
    static ScalarType L2_Diff(const ScalarType * input1_devptr, const ScalarType * intpu2_devptr, const size_t size, int numBlocks, cudaStream_t & stream);
    static void solve_p_in_nspca(double * devp, const size_t N, const size_t P, const size_t p,  double * ZtA, int * restriction, const double lambda, const double scale_square, cudaStream_t & stream);
    static void setConstant(ScalarType * data, const size_t size, const ScalarType value, int numBlocks, cudaStream_t& stream);
};

template<typename ScalarType, unsigned int numThreads>
CUDA_KERNEL void reduceSum(ScalarType *out, const ScalarType *in, size_t N );

template class GPUAlgorithm<double, 4, 4>;
template class GPUAlgorithm<double, 16, 4>;

#endif //NSPCA_GPUALGORITHM_H
