//
// Created by user on 7-11-16.
//

#ifndef NSPCA_GVIEW_H
#define NSPCA_GVIEW_H

#include <cuda_runtime.h>
#include <cstdlib>
#include "macro.h"

template<typename Scalar>
class GView {
public:
    Scalar * devPtr;
    size_t nRow;
    size_t nCol;
    CUDA_DEVICE GView(Scalar *devPtr, size_t nRow, size_t nCol);
    CUDA_DEVICE Scalar& operator()(const size_t i, const size_t j);
    CUDA_DEVICE const Scalar& operator()(const size_t i, const size_t j) const ;
};



template class GView<double>;
template class GView<int>;


#endif //NSPCA_GVIEW_H
