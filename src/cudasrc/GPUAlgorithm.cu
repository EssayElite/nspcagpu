//
// Created by user on 8-11-16.
//

#include "GPUAlgorithm.h"
#include "macro.h"
#include "GView.cuh"
#include <cassert>
#include <cuda_runtime.h>

template<typename ScalarType, unsigned int numThreads>
__global__ void setConstant_kernel(ScalarType* data, const size_t size, const ScalarType value){
    unsigned int tid = threadIdx.x+blockIdx.x*numThreads;

    for (;tid<size;tid+=blockDim.x*numThreads){
        data[tid]=value;
    }
};
__device__ __forceinline__ void solve_p_positive(double * ZtA, double * Pptr, const size_t N, const size_t P, const size_t p,
                                                 const int i, const int j, const double lambda, const double scale_square){
    GView<double> ZtAview(ZtA, P,p);
    GView<double> PView(Pptr, p,P);


    double t = (2*ZtAview(j,i)-lambda);
    if (t > 0){
        PView(i,j) = t/2*scale_square;

    }

}

__device__ __forceinline__ void solve_p_negative(double * ZtA, double * Pptr, const size_t N, const size_t P, const size_t p,
                                                 const int i, const int j, const double lambda, const double scale_square) {

    GView<double> ZtAview(ZtA, P,p);
    GView<double> PView(Pptr, p,P);


    double t = (2*ZtAview(j,i)+lambda);
    if (t < 0){
        PView(i,j) = t/(2*scale_square);

    }
}

__device__ __forceinline__ void solve_p_general(double * ZtA, double * Pptr, const size_t N, const size_t P, const size_t p,
                                                const int i, const int j, const double lambda, const double scale_square){
    GView<double> ZtAview(ZtA, P,p);
    GView<double> PView(Pptr, p,P);

    double t = (2*ZtAview(j,i)-lambda);
    if (t > 0){
        PView(i,j) = t/(2*scale_square);
    }
    else {
        t = (2*ZtAview(j,i)+lambda);{
            if (t<0){
                PView(i,j)=t/(2*scale_square);
            }
        }
    }
}

template<typename ScalarType, unsigned int numThreads>
__global__ void solve_p_in_nspca(double * devp, const size_t N, const size_t P, const size_t p,  double * ZtA,
                      int * restriction, const double lambda, const double scale_square ){
    const int tid = threadIdx.x;
    const int offset = numThreads*blockIdx.x;
    GView<int> resView(restriction, p, P);
    for (int index = tid+offset;index <p*P;index+=numThreads*blockDim.x){
        int j = index/p;
        int i = index - j*p;

        if (resView(i,j)==2){
            solve_p_general(ZtA, devp, N, P, p, i, j, lambda, scale_square);

        }
        else if (resView(i,j)==1){
            solve_p_positive(ZtA, devp, N, P, p, i, j, lambda, scale_square);
        }
        else if (resView(i,j)==-1){
            solve_p_negative(ZtA, devp, N, P, p, i, j, lambda, scale_square);
        }
    }
};


template<typename ScalarType, unsigned int numThreads>
CUDA_KERNEL void frobenius_kernel(ScalarType * result_dev_ptr, const ScalarType * input1_devptr, const size_t size){
    extern __shared__ ScalarType sPartials[];
    const  int tid = threadIdx.x;
    double sum = 0.0;
    for (size_t i = tid+numThreads*blockIdx.x; i < size; i += numThreads * blockDim.x) {
        ScalarType temp = input1_devptr[i];
        printf("This is from thread %d with value %0.12lf \n", i, temp);
        sum += temp*temp;
    }
    sPartials[tid] = sum;
    printf("This is thread %d, block %d, and value %0.12lf after calculating the sum \n", tid,  blockIdx.x, sPartials[tid]);
    __syncthreads();

    if (numThreads >= 1024) {
        if (tid < 512) {
            sPartials[tid] += sPartials[tid + 512];
        }
        __syncthreads();
    }
    if (numThreads >= 512) {
        if (tid < 256) {
            sPartials[tid] += sPartials[tid + 256];
        }
        __syncthreads();
    }
    if (numThreads >= 256) {
        if (tid < 128) {
            sPartials[tid] += sPartials[tid + 128];
        }
        __syncthreads();
    }
    if (numThreads >= 128) {
        if (tid < 64) {
            sPartials[tid] += sPartials[tid + 64];
        }
        __syncthreads();
    }
    // warp synchronous at the end
    if (tid < 32) {
        volatile double *wsSum = sPartials;

        if (numThreads >= 64) { wsSum[tid] += wsSum[tid + 32]; }
        if (numThreads >= 32) { wsSum[tid] += wsSum[tid + 16]; }
        if (numThreads >= 16) { wsSum[tid] += wsSum[tid + 8]; }
        if (numThreads >= 8) { wsSum[tid] += wsSum[tid + 4]; }
        if (numThreads >= 4) { wsSum[tid] += wsSum[tid + 2]; }
        if (numThreads >= 2) { wsSum[tid] += wsSum[tid + 1]; }
        if (tid == 0) {

            result_dev_ptr[blockIdx.x] = wsSum[0];
            printf("This is after reduction for block %d with value  %0.12lf \n ", blockIdx.x, wsSum[0]);
        }
    }

}


template<typename ScalarType, unsigned int numThreads>
CUDA_KERNEL void l2_diff_kernel(ScalarType * result_dev_ptr, const ScalarType * input1_devptr, const ScalarType * input2_devptr, const size_t size){
    extern __shared__ ScalarType sPartials[];
    const unsigned int tid = threadIdx.x;
    double sum = 0.0;
    for (size_t i = tid+numThreads*blockIdx.x; i < size; i += numThreads * blockDim.x) {
        ScalarType temp = input1_devptr[i]- input2_devptr[i];
        sum += temp*temp;
//        printf("from Thread %d the value is %0.12lf \n", tid, temp);
    }
    sPartials[tid] = sum;
    __syncthreads();
    if (numThreads >= 1024) {
        if (tid < 512) {
            sPartials[tid] += sPartials[tid + 512];
        }
        __syncthreads();
    }
    if (numThreads >= 512) {
        if (tid < 256) {
            sPartials[tid] += sPartials[tid + 256];
        }
        __syncthreads();
    }
    if (numThreads >= 256) {
        if (tid < 128) {
            sPartials[tid] += sPartials[tid + 128];
        }
        __syncthreads();
    }
    if (numThreads >= 128) {
        if (tid < 64) {
            sPartials[tid] += sPartials[tid + 64];
        }
    }
    __syncthreads();
    // warp synchronous at the end
    if (tid < 32) {
        volatile ScalarType *wsSum = sPartials;
        if (numThreads >= 64) { wsSum[tid] += wsSum[tid + 32]; }
        if (numThreads >= 32) { wsSum[tid] += wsSum[tid + 16]; }
        if (numThreads >= 16) { wsSum[tid] += wsSum[tid + 8]; }
        if (numThreads >= 8) { wsSum[tid] += wsSum[tid + 4]; }
        if (numThreads >= 4) { wsSum[tid] += wsSum[tid + 2]; }
        if (numThreads >= 2) { wsSum[tid] += wsSum[tid + 1]; }
        if (tid == 0) {
            result_dev_ptr[blockIdx.x] = wsSum[0];
        }
    }
}

template<typename ScalarType, unsigned int numThreads>
CUDA_KERNEL void
reduceSum(ScalarType *out, const ScalarType *in, size_t N )
{
    extern __shared__ ScalarType sPartials[];
    const unsigned int tid = threadIdx.x;
    double sum =0.0;
    for ( size_t i = blockIdx.x*numThreads + tid;
          i < N;
          i += numThreads*gridDim.x )
    {
        sum += in[i];
        printf("In reduction the values are %d \n", in[i]);
    }
    sPartials[tid] = sum;
    __syncthreads();

    if (numThreads >= 1024) {
        if (tid < 512) {
            sPartials[tid] += sPartials[tid + 512];
        }
        __syncthreads();
    }
    if (numThreads >= 512) {
        if (tid < 256) {
            sPartials[tid] += sPartials[tid + 256];
        }
        __syncthreads();
    }
    if (numThreads >= 256) {
        if (tid < 128) {
            sPartials[tid] += sPartials[tid + 128];
        }
        __syncthreads();
    }
    if (numThreads >= 128) {
        if (tid <  64) {
            sPartials[tid] += sPartials[tid +  64];
        }
        __syncthreads();
    }
    // warp synchronous at the end
    if ( tid < 32 ) {
        volatile ScalarType *wsSum = sPartials;
        if (numThreads >=  64) { wsSum[tid] += wsSum[tid + 32]; }
        if (numThreads >=  32) { wsSum[tid] += wsSum[tid + 16]; }
        if (numThreads >=  16) { wsSum[tid] += wsSum[tid +  8]; }
        if (numThreads >=   8) { wsSum[tid] += wsSum[tid +  4]; }
        if (numThreads >=   4) { wsSum[tid] += wsSum[tid +  2]; }
        if (numThreads >=   2) { wsSum[tid] += wsSum[tid +  1]; }
        if ( tid == 0 ) {
            out[blockIdx.x] = sPartials[0];
        }
    }
}

template<typename ScalarType, unsigned int numThreads, unsigned int numThreadsForReduction>
ScalarType GPUAlgorithm<ScalarType, numThreads, numThreadsForReduction>::Frobenius(
                                                                           const ScalarType *input1_devptr,
                                                                           const size_t size, int numBlocks,
                                                                           cudaStream_t &stream) {
    ScalarType result = 0;
    ScalarType * dev_partial;
    ScalarType * answer;
    HANDLE_ERROR(cudaMalloc((void**)&dev_partial,sizeof(ScalarType)*numBlocks));
    HANDLE_ERROR(cudaMalloc((void**)&answer, sizeof(ScalarType)));
    frobenius_kernel<ScalarType, numThreads><<<numBlocks, numThreads, numThreads*sizeof(ScalarType), stream>>>(
            dev_partial, input1_devptr, size);

    reduceSum<ScalarType, numThreadsForReduction><<<1, numThreadsForReduction,numThreadsForReduction*sizeof(ScalarType), stream>>>(
            answer, dev_partial, numThreadsForReduction);
    cudaStreamSynchronize(stream);
    HANDLE_ERROR(cudaMemcpy(&result, answer, sizeof(ScalarType), cudaMemcpyDeviceToHost));
    HANDLE_ERROR(cudaFree(answer));
    HANDLE_ERROR(cudaFree(dev_partial));
    return result;
}

template<typename ScalarType, unsigned int numThreads, unsigned int numThreadsForReduction>
ScalarType GPUAlgorithm<ScalarType, numThreads, numThreadsForReduction>::L2_Diff(
                                                                                 const ScalarType *input1_devptr,
                                                                                 const ScalarType *intpu2_devptr,
                                                                                 const size_t size, int numBlocks,
                                                                                 cudaStream_t &stream) {
    double result=0.0;
    double * dev_partial;
    ScalarType * gpu_answer;
    HANDLE_ERROR(cudaMalloc((void**)&dev_partial,sizeof(ScalarType)*numBlocks));
    HANDLE_ERROR(cudaMalloc((void**)&gpu_answer, sizeof(ScalarType)));
    l2_diff_kernel<ScalarType, numThreads><<<numBlocks, numThreads, numThreads*sizeof(ScalarType), stream>>>(
            dev_partial, input1_devptr, intpu2_devptr, size);
    reduceSum<ScalarType, numThreadsForReduction><<<1, numThreadsForReduction,numThreadsForReduction*sizeof(ScalarType), stream>>>(
            gpu_answer, dev_partial, numThreadsForReduction);
    cudaStreamSynchronize(stream);
    HANDLE_ERROR(cudaMemcpy(&result, gpu_answer, sizeof(ScalarType), cudaMemcpyDeviceToHost));
    HANDLE_ERROR(cudaFree(gpu_answer));
    HANDLE_ERROR(cudaFree(dev_partial));
    return result;
}

template<typename ScalarType, unsigned int numThreads, unsigned int numThreadsForReduction>
void GPUAlgorithm<ScalarType, numThreads, numThreadsForReduction>::setConstant(ScalarType * data, const size_t size, const ScalarType value, int numBlocks, cudaStream_t& stream){
    assert(numThreads*numBlocks<size&&"Too many threads for current size");
    setConstant_kernel<ScalarType, numThreads><<<numBlocks, numThreads, 0, stream>>>(data, size, value);
};