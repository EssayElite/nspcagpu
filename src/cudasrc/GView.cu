//
// Created by user on 7-11-16.
//

#include "GView.cuh"
#include "macro.h"
#include <cuda_runtime.h>

template<typename Scalar>
CUDA_DEVICE GView<Scalar>::GView(Scalar *_devPtr, size_t nRow, size_t nCol) : devPtr(_devPtr), nRow(nRow), nCol(nCol) {}
template<typename Scalar>
CUDA_DEVICE Scalar &GView<Scalar>::operator()(const size_t i, const size_t j) {
    return devPtr[__IC__(i,j,nRow,nCol)];
}
template<typename Scalar>
CUDA_DEVICE const Scalar &GView<Scalar>::operator()(const size_t i, const size_t j) const {
    return devPtr[__IC__(i,j, nRow, nCol)];
}

