//
// Created by user on 4-11-16.
//

#ifndef NSPCA_FORTRANLIB_H
#define NSPCA_FORTRANLIB_H
extern "C" {
    void gramschmidt_(double *, int *, int*);
    void cumulate_cpu_(int* data, int * count, int *, int *);
};
#endif //NSPCA_FORTRANLIB_H
