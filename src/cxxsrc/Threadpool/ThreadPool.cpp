//
// Created by user on 19-7-16.
//

#include "include/ThreadPoolMod/ThreadSafeQueue.h"
#include "include/ThreadPoolMod/ThreadJoin.h"
#include "include/ThreadPoolMod/FunctionWrapper.hpp"
#include "include/ThreadPoolMod/ThreadPool.h"
#include <mutex>
#include <thread>
#include <condition_variable>
#include <vector>
#include <functional>
#include <atomic>
#include <future>
#include <iostream>


void threadpool::worker_thread()
    {
        while(!done)
        {
            function_wrapper task;
            if(work_queue.try_pop(task))
            {
                task();
            }
            else
            {
                std::this_thread::yield();
            }
        }
    }

threadpool::threadpool(const unsigned int thread_count):done(false),joiner(threads)
    {
        //unsigned const thread_count=std::thread::hardware_concurrency()
        try
        {
            for(unsigned i=0;i<thread_count;++i)
            {
                threads.push_back(
                        std::thread(&threadpool::worker_thread,this));
            }
        }
        catch(...) //Catch all exceptions
        {
            done=true;
            throw;
        }
    }

threadpool::~threadpool()
    {
        done=true;
    }

double threadpool::getNumThreads() {
    return this->threads.size();
}

template<typename FunctionType>
std::future<typename std::result_of<FunctionType()>::type> threadpool::submit(FunctionType f)
    {
//        std::cout << "Work has been submitted\n";
        typedef typename std::result_of<FunctionType()>::type
                result_type;
        std::packaged_task<result_type()> task(std::move(f));
        std::future<result_type> res(task.get_future());
        work_queue.push(std::move(task));
        return res;
    }



