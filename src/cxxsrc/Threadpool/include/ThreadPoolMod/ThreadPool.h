//
// Created by user on 19-7-16.
//

#ifndef CUDATRIAL_THREADPOOL_H
#define CUDATRIAL_THREADPOOL_H

#include <mutex>
#include <thread>
#include <condition_variable>
#include <vector>
#include <functional>
#include <atomic>
#include <future>

#include "ThreadJoin.h"
#include "ThreadSafeQueue.h"
#include "FunctionWrapper.hpp"
class threadpool {
    std::atomic_bool done;
    threadsafe_queue<function_wrapper> work_queue;
    std::vector<std::thread> threads;
    join_thread joiner;

    void worker_thread();
public:
    threadpool(const unsigned int);
    ~threadpool();
    double getNumThreads();
    template<typename FunctionType>
    std::future<typename std::result_of<FunctionType()>::type>
            submit(FunctionType f);
};
#endif //CUDATRIAL_THREADPOOL_H
