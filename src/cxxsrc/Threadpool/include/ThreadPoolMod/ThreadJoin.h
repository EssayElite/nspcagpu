//
// Created by user on 19-7-16.
//

#ifndef CUDATRIAL_THREADJOIN_H
#define CUDATRIAL_THREADJOIN_H
#include <thread>
#include <vector>


class join_thread{
    std::vector<std::thread>& threads;
public:
    explicit join_thread(std::vector<std::thread>& other);
    ~join_thread();
};
#endif //CUDATRIAL_THREADJOIN_H
