//
// Created by user on 7-11-16.
//

#ifndef NSPCA_IO_HELPER_H
#define NSPCA_IO_HELPER_H

#include "CMatrix.h"
#include "string.h"
#include <iostream>
#include <string>
#include <Eigen/Dense>
using std::string;
#include <vector>
using std::vector;
template<typename ScalarType>
class io_helper {
public:
    static  Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic> read_CMatrixCM(const std::string& inpath);
    template<typename T>
    static bool write_CMatrix(const T & out, const std::string& outpaht);
};

std::vector<std::string> getVectorStringfromFile(const std::string &inpath);
vector<string> split(string data, string token);

bool test_input();
#endif //NSPCA_IO_HELPER_H
