//
// Created by user on 4-11-16.
//

#include "random.h"
#include <cassert>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <iostream>
#include "time.h"

template<typename ScalerType>
Random<ScalerType>::Random() {
    srand(time(NULL));
    T = gsl_rng_default;
    r = gsl_rng_alloc (T);
    gsl_rng_set(r, rand());
}

template<typename ScalerType>
Random<ScalerType>::~Random() {
    gsl_rng_free (r);
}

template<typename ScalerType>
void Random<ScalerType>::normal(ScalerType *dst, const size_t size, const double mean, const double std) {
    assert(size>0);
    for (int i=0;i<size;i++){
        dst[i]=(ScalerType)(gsl_ran_gaussian(r, std)+mean);
    }
    }