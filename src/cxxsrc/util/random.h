//
// Created by user on 4-11-16.
//

#ifndef NSPCA_RANDOM_H
#define NSPCA_RANDOM_H

#include <cstdlib>
#include <gsl/gsl_rng.h>

template<typename ScalerType>
class Random {
private:
    const gsl_rng_type * T;
    gsl_rng * r;
public:
    Random();
    ~Random();
    void normal(ScalerType* dst, const size_t size, const double mean, const double std);
};



template class Random<double>;
#endif //NSPCA_RANDOM_H
