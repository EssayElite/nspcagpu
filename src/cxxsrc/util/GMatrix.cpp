//
// Created by user on 7-11-16.
//
#include "GMatrix.h"
#include <Eigen/Dense>
using namespace Eigen;
#include "DataTransformer.h"
template<typename Scalar>
GMatrix<Scalar>::GMatrix(const size_t _nRow, const size_t _nCol):nRow(_nRow), nCol(_nCol){
    cudaMalloc((void**)&dev_ptr, sizeof(Scalar)*nRow*nCol);
}
template<typename Scalar>
GMatrix<Scalar>::~GMatrix() {
    cudaFree(dev_ptr);
}
template<typename Scalar>
size_t GMatrix<Scalar>::rows() const {
    return nRow;
}
template<typename Scalar>
size_t GMatrix<Scalar>::cols() const {
    return nCol;
}
template<typename Scalar>
Scalar *GMatrix<Scalar>::data() {
    return dev_ptr;
}
template<typename Scalar>
const Scalar *GMatrix<Scalar>::data() const {
    return dev_ptr;
}

template<typename Scalar>
void GMatrix<Scalar>::print(cudaStream_t &stream) const {
    const size_t nRow = rows();
    const size_t nCol = cols();

    Matrix<Scalar, Dynamic, Dynamic> cpu_mat_temp = Matrix<Scalar, Dynamic, Dynamic>::Constant(nRow, nCol, 0);
    Scalar * host_ptr_cpu_mat_temp = (Scalar*)cpu_mat_temp.data();
    cudaMemcpyAsync(host_ptr_cpu_mat_temp, data(), sizeof(Scalar)*nRow*nCol, cudaMemcpyDeviceToHost, stream);
    cudaStreamSynchronize(stream);
    std::cout << cpu_mat_temp << std::endl;
}
template<typename Scalar>
GMatrix<Scalar>::GMatrix(const Matrix<Scalar, Dynamic, Dynamic> &src, cudaStream_t & stream) {
    nRow = src.rows(); nCol= src.cols();
    cudaMalloc((void**)&dev_ptr, sizeof(Scalar)*nRow*nCol);
    DataTransformer<Scalar, Matrix<Scalar, Dynamic, Dynamic>>::copyCPUtoGPU(src, *this, stream);
}





