//
// Created by user on 4-11-16.
//

#ifndef NSPCA_EXCEPTION_H
#define NSPCA_EXCEPTION_H

#include <cstdlib>
class exception {};

class DimensionMisMatch:public exception{
public:
    DimensionMisMatch(size_t lhs_row, size_t lhs_col, size_t rhs_row, size_t rhs_col, std::string lhs_name,
                          std::string rhs_name);
    virtual void print_err();
    virtual void print_dimension();
    virtual void print_err_type()=0;
    size_t getLhs_row() const;

    size_t getLhs_col() const;

    size_t getRhs_row() const;

    size_t getRhs_col() const;

    size_t lhs_row;
    size_t lhs_col;

    size_t rhs_row;
    size_t rhs_col;

    std::string lhs_name;
    std::string rhs_name;
};

class DimensionMisMatchMatMul:public DimensionMisMatch{
public:
    DimensionMisMatchMatMul(size_t lhs_row, size_t lhs_col, size_t rhs_row, size_t rhs_col,std::string lhs_name, std::string rhs_name);
    virtual void print_err_type();
};

class DimensionMisMatchMatrixCopy: public DimensionMisMatch{
public:
    DimensionMisMatchMatrixCopy(size_t lhs_row, size_t lhs_col, size_t rhs_row, size_t rhs_col, std::string, std::string);
    virtual void print_err_type();

};

#endif //NSPCA_EXCEPTION_H
