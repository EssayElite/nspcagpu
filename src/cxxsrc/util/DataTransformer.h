//
// Created by user on 7-11-16.
//

#ifndef NSPCA_DATATRANSFORMER_H
#define NSPCA_DATATRANSFORMER_H
#include <cstdlib>
#include "GMatrix.h"
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include <Eigen/Dense>

template<typename Scalar, typename MatrixType>
class DataTransformer{
public:
    static bool copyGPUtoCPU(const Scalar * devPtr, size_t nRow, size_t nCol, Scalar* hostPtr, cudaStream_t & stream);

    static bool copyGPUtoCPU(const GMatrix<Scalar> & from, MatrixType& to, cudaStream_t &  stream);
    static bool copyCPUtoGPU(const Scalar * hostPtr, size_t nRow, size_t nCol, Scalar * devPtr, cudaStream_t& stream);
    static bool copyCPUtoGPU(const MatrixType& from, GMatrix<Scalar> & to, cudaStream_t&  stream);
private:
    template<typename T, typename T2>
    static bool check_dimension(const T& lhs, const T2 & rhs);
};

bool test_copy_from_cpu_and_back();



template class DataTransformer<double,Eigen::MatrixXd>;
template class DataTransformer<int, Eigen::MatrixXi>;


#endif //NSPCA_DATATRANSFORMER_H
