//
// Created by user on 7-11-16.
//

#include "io_helper.h"
#include <iostream>
using std::cout;
using std::cin;
using std::endl;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iterator>
using std::istream_iterator;
#include <algorithm>
using std::copy;

#include <Eigen/Dense>
template<typename ScalarType>
Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic> io_helper<ScalarType>::read_CMatrixCM(const std::string& inpath){
    vector<string> tmp_result = getVectorStringfromFile(inpath);
    vector<string> first_row = split(tmp_result[0], ",");
    size_t nRow = tmp_result.size();
    size_t nCol = first_row.size();

    Eigen::Matrix<ScalarType, Eigen::Dynamic, Eigen::Dynamic> result(nRow, nCol);
    for (size_t i =0;i<result.rows();i++){
        first_row = split(tmp_result[i], ",");

        for(size_t j=0;j<result.cols();j++){
            result(i,j)=(ScalarType)stod(first_row[j]);
        }
    }
    return result;
}

std::vector<std::string> getVectorStringfromFile(const std::string &inpath)
{
    ifstream input_stream(inpath);
    std::vector<std::string> result;

    copy(istream_iterator<string>(input_stream),
         istream_iterator<string>(),
         back_inserter(result));

    return result;
}

template<typename ScalarType>
template<typename T>
bool io_helper<ScalarType>::write_CMatrix(const T & out, const std::string& outpath){
    ofstream outstream;
    outstream.open(outpath);
    for (size_t i=0;i<out.rows();i++){
        outstream << out(i,0);
        for (size_t j=1;j<out.cols();j++){
            outstream << "," << out(i,j) ;

        }
        outstream << "\n";
    }
    return true;
}


vector<string> split(string data, string token)
{
    vector<string> output;
    size_t pos = string::npos; // size_t to avoid improbable overflow
    do
    {
        pos = data.find(token);
        output.push_back(data.substr(0, pos));
        if (string::npos != pos)
            data = data.substr(pos + token.size());
    } while (string::npos != pos);
    return output;
}

bool test_input(){
    string file = "/home/user/Desktop/input.txt";
    string outfile = "/home/user/Desktop/output.txt";
    Eigen::MatrixXi result=io_helper<int>::read_CMatrixCM(file);
    bool bool_var = io_helper<int>::write_CMatrix(result, outfile);
    return true;
}
