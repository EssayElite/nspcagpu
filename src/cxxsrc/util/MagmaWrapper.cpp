//
// Created by user on 10-11-16.
//

#include "MagmaWrapper.h"

void
MagmaWrapper::matmulNN(GMatrix<double> &out, const GMatrix<double> &lhs, const GMatrix<double> &rhs, double value1, double value2,
                       magma_queue_t &queue) {
    magma_dgemm_q(MagmaNoTrans, MagmaNoTrans, out.rows(), out.cols(), lhs.cols(), value1, lhs.data(), lhs.rows(), rhs.data(), lhs.rows(), value2, out.data(),
                  out.rows(), queue);
}

void MagmaWrapper::matmulNT(GMatrix<double> &out, const GMatrix<double> &lhs, const GMatrix<double> &rhs, double value1,
                            double value2, magma_queue_t &queue) {
    magma_dgemm_q(MagmaNoTrans, MagmaTrans, out.rows(), out.cols(), lhs.cols(), value1, lhs.data(), lhs.rows(), rhs.data(), lhs.cols(), value2, out.data(),
                  out.rows(), queue);
}

void MagmaWrapper::matmulTT(GMatrix<double> &out, const GMatrix<double> &lhs, const GMatrix<double> &rhs, double value1,
                            double value2, magma_queue_t &queue) {
    magma_dgemm_q(MagmaTrans, MagmaTrans, out.rows(), out.cols(), lhs.rows(), value1, lhs.data(), lhs.cols(), rhs.data(), lhs.rows(), value2, out.data(),
                  out.rows(), queue);
}

void MagmaWrapper::matmulTN(GMatrix<double> &out, const GMatrix<double> &lhs, const GMatrix<double> &rhs, double value1,
                            double value2, magma_queue_t &queue) {
    magma_dgemm_q(MagmaTrans, MagmaNoTrans, out.rows(), out.cols(), lhs.rows(), value1, lhs.data(), lhs.rows(), rhs.data(), lhs.rows(), value2, out.data(),
                  out.rows(), queue);
}
