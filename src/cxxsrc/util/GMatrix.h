#ifndef NSPCA_GMATRIX_CU
#define NSPCA_GMATRIX_CU

#include <cstdlib>
#include <cuda_runtime_api.h>
#include <cuda_runtime.h>
#include "CMatrix.h"
#include <Eigen/Dense>
using namespace Eigen;
template<typename Scalar>
class GMatrix {
public:

    Scalar * dev_ptr;
    size_t nRow;
    size_t nCol;
    GMatrix(const size_t _nRow, const size_t _nCol);
    GMatrix(const Matrix<Scalar, Dynamic, Dynamic> & src, cudaStream_t & stream);
    ~GMatrix();
    size_t rows() const;

    size_t cols() const;
    Scalar * data();
    const Scalar * data() const;

    void print(cudaStream_t & stream) const;
};



template class GMatrix<int>;
template class GMatrix<double>;


#endif