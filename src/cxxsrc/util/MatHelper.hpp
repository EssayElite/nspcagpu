//
// Created by user on 8-11-16.
//

#ifndef NSPCA_MATHELPER_H
#define NSPCA_MATHELPER_H
#include <Eigen/Dense>
using namespace Eigen;
class MatHelper{
public:

    static void standardize(MatrixXd& X){
        for (int col =0;col<X.cols();col++){
            double mean = average(X.col(col));
            double std = sqrt(variance(X.col(col)));
            for (int row=0;row<X.rows();row++){
                X(row, col) = (X(row, col)-mean)/std;
            }
        }
    }

    static double variance(const VectorXd & v){
        double result= 0.0;
        double mean = average(v);
        for (int i =0 ;i<v.rows();i++){
            result += (v(i)-mean)*(v(i)-mean);
        }

        return result/v.rows();
    }
    static double average(const VectorXd &v){
        double result = 0.0;
        for (int i = 0;i<v.rows();i++){
            result+= v(i);
        }
        return result/v.rows();
    }


    static void rescale(MatrixXd & matrix, const double scale){
        for (int i=0;i<matrix.rows();i++){
            for (int j=0;j<matrix.cols();j++)
            {
                matrix(i,j)=matrix(i,j)*scale;
            }
        }
    }

};


#endif //NSPCA_MATHELPER_H
