//
// Created by user on 9-11-16.
//

#include "AfHelper.h"
template<typename Scalar>
void AfHelper<Scalar>::copyFromEigen(const Matrix<Scalar, Dynamic, Dynamic> &from, af::array &to) {
    const Scalar * eigen_ptr = (Scalar *) from.data();
    af::array temp(from.rows(), from.cols(), eigen_ptr, afHost);
    to = temp;
}
template<typename Scalar>
void AfHelper<Scalar>::copyToEigen(Matrix<Scalar, Dynamic, Dynamic> &to, const af::array & from) {
    Scalar * eigen_ptr = (Scalar * ) to.data();
    from.host(eigen_ptr);
}
