//
// Created by user on 10-11-16.
//

#ifndef NSPCA_MAGMAWRAPPER_H
#define NSPCA_MAGMAWRAPPER_H
#include <magma.h>
#include "GMatrix.h"

class MagmaWrapper {
public:
    static void matmulNN(GMatrix<double> & out, const GMatrix<double> & lhs, const GMatrix<double>& rhs, double value1, double value2, magma_queue_t & queue);
    static void matmulNT(GMatrix<double> & out, const GMatrix<double> & lhs, const GMatrix<double>& rhs, double value1, double value2, magma_queue_t & queue);
    static void matmulTT(GMatrix<double> & out, const GMatrix<double> & lhs, const GMatrix<double>& rhs, double value1, double value2, magma_queue_t & queue);
    static void matmulTN(GMatrix<double> & out, const GMatrix<double> & lhs, const GMatrix<double>& rhs, double value1, double value2, magma_queue_t & queue);
};


#endif //NSPCA_MAGMAWRAPPER_H
