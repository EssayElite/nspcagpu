//
// Created by user on 4-11-16.
//

#include "CMatrix.h"
#include <Eigen/Dense>

template<typename T1, typename Derived2>
void mul_constant(const T1 scale, CMatrix<T1, Derived2>& out){
    for (int i=0;i<out.rows()*out.cols();i++){
        out.data[i] *=scale;
    }
};
