//
// Created by user on 2-11-16.
//

#ifndef NSPCA_CMATRIX_H
#define NSPCA_CMATRIX_H

#include <cassert>
#include <ostream>
#include <iostream>
using std::cout; using std::endl;
#include "exception.h"
#include "internal.h"
#include <cstdlib>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
template<typename ScalerType, typename Derived>
class Base;

template<typename ScalerType, typename Derived>
class Operation;

template<typename ScalerType, typename LHS, typename RHS>
class Add_Op:public Operation<ScalerType, Add_Op<ScalerType, LHS, RHS>>{
public:
    using Operation<ScalerType, Add_Op<ScalerType, LHS, RHS>>::operator+;
    Add_Op(const LHS &lhs, const RHS &rhs) : lhs(lhs), rhs(rhs) {
        std::cout << "Dimension check in add_op is not yet implemented "  << std::endl;
        std::cout << "Type selection in add_op is not yet implemented "  << std::endl;
    }
    size_t rows() const {
        return lhs.rows();
    }
    size_t cols() const {
        return lhs.cols();
    }
    const ScalerType& get(const size_t i, const size_t j) const {
        return lhs.get(i,j)+rhs.get(i,j);
    }
    const LHS & lhs;
    const RHS & rhs;

};

template<typename LHS, typename RHS>
class assign_op {
public:
    static LHS & run(LHS & lhs, const RHS & rhs){
        std::cout << "Dimension check in assignment operation is not implemented yet" << std::endl;
        for (int i=0;i<lhs.rows();i++){
            for (int j=0;j<lhs.rows();j++){
                lhs(i,j) = rhs(i,j);
            }
        }
        return lhs;
    }
};
template<typename ScalerType, typename Derived>
class Base {
public:
    Derived& derived(){
        return *static_cast<Derived*>(this);
    }
    const Derived& derived() const {
        return *static_cast<const Derived*>(this);
    }

    template<typename otherScalertype, typename otherDerived>
    const Add_Op<select_type_host<ScalerType, otherScalertype>, Derived, otherDerived> operator+(const Base<otherScalertype, otherDerived> & rhs) const {
        return Add_Op<select_type_host<ScalerType, otherScalertype>, Derived, otherDerived>(this->derived(), rhs.derived());
    }
};

/*
 * This is the class inherited by all matrix-related classes
 * including matrix class, view class, operation class etc.
 * */


template<typename ScalerType, typename Derived>
class MatBase: public Base<ScalerType, MatBase<ScalerType, Derived>>{
public:
    using super= Base<ScalerType, MatBase<ScalerType, Derived>>;
    using super::operator+;
    Derived& derived(){
        return *static_cast<Derived*>(this);
    }
    const Derived& derived() const {
        return *static_cast<const Derived*>(this);
    }
    template<typename otherDerived>
    Derived& operator=(const otherDerived & rhs) {
        assign_op<MatBase<ScalerType, Derived>, otherDerived>::run(*this, rhs);
        return derived();
    }
    ScalerType& operator()(const size_t i, const size_t j){
        return derived().operator()(i, j);
    }
    const ScalerType& operator()(const size_t i, const size_t j) const {
        return derived().operator()(i,j);
    }
    size_t rows() const {
        return derived().rows();
    }

    size_t cols() const {
        return derived().cols();
    }
};

template<typename ScalerType, typename Derived>
class Operation: public Base<ScalerType, Operation<ScalerType, Derived>> {
public:
    using Base<ScalerType, Operation<ScalerType, Derived>>::operator+;
    Derived& derived(){
        return *static_cast<Derived*>(this);
    }
    const Derived& derived() const {
        return *static_cast<const Derived*>(this);
    }
    ScalerType& operator()(const size_t i, const size_t j){
        return derived().operator()(i, j);
    }
    const ScalerType& operator()(const size_t i, const size_t j) const {
        return derived().operator()(i,j);
    }

};


template<typename ScalerType, typename Derived>
class CMatrix:public MatBase<ScalerType, CMatrix<ScalerType, Derived>>{
protected:
    size_t nRow;
    size_t nCol;
public:
    ScalerType* data;
    using MatBase<ScalerType, CMatrix<ScalerType, Derived>>::operator=;
    using MatBase<ScalerType, CMatrix<ScalerType, Derived>>::operator+;

    template<typename OtherScalertype, typename otherDerived>
    const Add_Op<ScalerType, CMatrix<ScalerType, Derived>, Base<OtherScalertype, otherDerived>> operator+(const Base<OtherScalertype, otherDerived> & other) const {
        return Add_Op<ScalerType, CMatrix<ScalerType, Derived>, Base<OtherScalertype, otherDerived>>(*this, other);
    }
    CMatrix(const size_t _nRow, const size_t _nCol){
        nRow= _nRow;
        nCol= _nCol;
        cudaMallocHost((void**)&data, sizeof(ScalerType)*nRow*nCol);

    }
    ~CMatrix(){
        cudaFreeHost(data);
    }
    Derived& derived(){
        return *static_cast<Derived*>(this);
    }
    const Derived& derived() const {
        return *static_cast<const Derived*>(this);
    }
    ScalerType get(const size_t i, const size_t j) const {
        return derived().get(i,j);
    }
    ScalerType * getPtr(){
        return data;
    }
    ScalerType * getPtr() const {
        return data;
    }

    size_t rows(){
        return nRow;
    }
    size_t cols(){
        return nCol;
    }

    size_t rows() const {
        return nRow;

    }
    size_t cols() const {
        return nCol;
    }

    ScalerType& operator()(const size_t i, const size_t j){
        return derived().operator()(i,j);
    }

    const ScalerType& operator()(const size_t i, const size_t j) const {
        return const_cast<ScalerType&>(
                const_cast<CMatrix<ScalerType, Derived>&>(*this).operator()(i,j));

    }

    void setConstant(const ScalerType value){
//        std::cout << "flag1 " << nRow*nCol << std::endl;
        for (int i=0;i<nRow*nCol;i++){
            data[i]=value;
        }


    }
    friend std::ostream& operator<<(std::ostream& os, const CMatrix<ScalerType, Derived>& dt){
        for (int row =0;row<dt.rows();row++){
            os << dt(row,0);
            for (int col = 1; col<dt.cols();col++){
                os << "," << dt(row,col);
            }
            os << "\r\n";
        }
        return os;
    }
    void print(){
        for (int row =0;row<this->rows();row++){
            std::cout << this->operator()(row, 0);
            for (int col = 1; col<this->cols();col++){
                std::cout << "," << this->operator()(row, col);
            }
            std::cout  << std::endl;
        }

    }
    template<typename T1, typename Derived2>
    friend void mul_constant(const T1, CMatrix<T1, Derived2>&);

};

template<typename ScalerType>
class CMatrixRM:public CMatrix<ScalerType, CMatrixRM<ScalerType>>{
public:
    using super = CMatrix<ScalerType, CMatrixRM<ScalerType>>;
    using super::operator=;
    using super::operator+;
    CMatrixRM(const size_t _nRow, const size_t _nCol):CMatrix<ScalerType, CMatrixRM<ScalerType>>(_nRow, _nCol){}
    ScalerType get(const size_t i, const size_t j) const {
        ScalerType result = super::data[j + super::nCol * i];
        return result;
    }
    ScalerType & operator()(const size_t i, const size_t j) {

        return super::data[j + super::nCol * i];
    }
};

template<typename ScalerType>
class CMatrixCM:public CMatrix<ScalerType, CMatrixCM<ScalerType>>{
public:
    using super = CMatrix<ScalerType, CMatrixCM<ScalerType>>;
    using super::operator=;
    using super::operator+;
    CMatrixCM(const size_t _nRow, const size_t _nCol):CMatrix<ScalerType, CMatrixCM<ScalerType>>(_nRow, _nCol){

    }

    ScalerType & operator()(const size_t i, const size_t j) {
//       std::cout <<j*super::nRow +  i << std::endl;
        return super::data[j*super::nRow + i];
    }
    const  ScalerType&  operator()(const size_t i, const size_t j) const{
        //std::cout << "From operator " << super::data[j*super::nRow+i] << std::endl;
        return super::data[j*super::nRow + i];
    }
    ScalerType get(const size_t i, const size_t j) const {
        ScalerType result = super::data[j*super::nRow + i];
        return result;
    }


};

template<typename ScalerType>
class CVector : public CMatrix<ScalerType, CVector<ScalerType>>{
public:
    using super =  CMatrix<ScalerType, CVector<ScalerType>>;

    CVector(const size_t _nRow):CMatrix<ScalerType, CVector<ScalerType>>(_nRow, 1){}
    ScalerType & operator()(const size_t i, const size_t j){
        return operator()(i);
    }
    ScalerType & operator()(const size_t i) {
        return super::data[i];
    }

    const ScalerType & operator()(const size_t i) const {
        return super::data[i];

    }

//    friend std::ostream operator<<(std::ostream& os, const CVector<ScalerType>& dt){
//        os << dt(0);
//        for (int i=1;i<dt.rows();i++){
//            os << ","<< dt(i);
//        }
//        os << "\r\n";
//    }
    void print(){
        std::cout << this->operator()(0);
        for (int i=1;i<this->rows();i++){
            std::cout << "," << this->operator()(i);
        }
        std::cout << std::endl;
    }
    void setConstant(const ScalerType value){
        for (int i=0;i<this->rows();i++){
            this->operator()(i)=value;
        }
    }
};

template class CMatrixCM<double>;
template class CMatrixCM<int>;
template class CVector<double>;
template class CVector<int>;

//template<typename ScalerType, typename Derived>
//void setEigen(Eigen::Matrix<ScalerType, Eigen::Dynamic, Eigen::Dynamic> & out,   const CMatrix<ScalerType, Derived>& in){
//    const int rows = out.rows();
//    const int cols = out.cols();
//    try {
//        if (rows != in.rows() || cols != in.cols()){
//            throw DimensionMisMatchMatrixCopy(out.rows(), out.cols(), in.rows(), in.cols(), "Destination", "Source");
//        }
//        else {
//            for (int i = 0; i < rows; i++) {
//                for (int j = 0; j < cols; j++) {
//                    out(i, j) = in(i, j);
//                }
//            }
//        }
//    }
//    catch (DimensionMisMatchMatrixCopy d) {
//            d.print_err();
//    }
//
//};
//
//template<typename ScalerType, typename Derived>
//void getEigen(CMatrix<ScalerType, Derived>& out, Eigen::Matrix<ScalerType, Eigen::Dynamic, Eigen::Dynamic> & in ){
//    const int rows = out.rows();
//    const int cols = out.cols();
//    try {
//        if (rows != in.rows() || cols != in.cols()){
//            throw DimensionMisMatchMatrixCopy(out.rows(), out.cols(), in.rows(), in.cols(), "Destination", "Source");
//        }
//        else {
//            for (int i = 0; i < rows; i++) {
//                for (int j = 0; j < cols; j++) {
//                    out(i, j) = in(i, j);
//                }
//            }
//        }
//    }
//    catch (DimensionMisMatchMatrixCopy d) {
//        d.print_err();
//    }
//
//};



template<typename ScalerType, typename Derived>
void matmul(CMatrix<ScalerType, Derived>& out, const CMatrix<ScalerType, Derived>& lhs, const CMatrix<ScalerType, Derived> & rhs ){
//    To do: dimensionality check
    try {
        if (lhs.cols()!=rhs.rows()){
            throw DimensionMisMatchMatMul(lhs.rows(), lhs.cols(), rhs.rows(), rhs.cols(),  "lhs", "rhs");
        }
        else if(out.rows() != lhs.rows()){
            throw DimensionMisMatchMatMul(out.rows(), out.cols(), lhs.rows(), lhs.cols(),  "output", "lhs");
        }
        else if(out.cols()!=rhs.cols()){
            throw DimensionMisMatchMatMul(out.rows(), out.cols(), rhs.rows(), rhs.cols(),  "output", "rhs");
        }
        else {
            for (int i = 0; i < out.rows(); i++) {
                for (int j = 0; j < out.cols(); j++) {
                    out(i, j) = 0;
                    for (int k = 0; k < lhs.cols(); k++) {
                        out(i, j) = out(i, j) + lhs(i, k) * rhs(k, j);
                    }
                }
            }
        }
    }
    catch (DimensionMisMatchMatMul d){
        d.print_err();
    }

};

#endif