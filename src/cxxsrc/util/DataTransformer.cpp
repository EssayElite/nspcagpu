//
// Created by user on 7-11-16.
//

#include "DataTransformer.h"

using std::cout; using std::endl;
template<typename Scalar, typename MatrixType>
template<typename T1, typename T2>
bool DataTransformer<Scalar, MatrixType>::check_dimension(const T1 & lhs, const T2 & rhs) {
    if (lhs.rows()==rhs.rows() && lhs.cols()==rhs.cols()){
        return true;
    }
    else{
        return false;
    }
}

template<typename Scalar, typename MatrixType>
bool DataTransformer<Scalar, MatrixType>::copyGPUtoCPU(const Scalar *devPtr, size_t nRow, size_t nCol, Scalar *hostPtr, cudaStream_t & stream) {
    cudaMemcpyAsync(hostPtr, devPtr, sizeof(Scalar)*nRow*nCol, cudaMemcpyDeviceToHost, stream);
    return true;
}


template<typename Scalar, typename MatrixType>
bool DataTransformer<Scalar, MatrixType>::copyCPUtoGPU(const Scalar *hostPtr, size_t nRow, size_t nCol, Scalar *devPtr, cudaStream_t &stream) {
    cudaMemcpyAsync(devPtr, hostPtr, sizeof(Scalar)*nRow*nCol, cudaMemcpyHostToDevice, stream);
    return true;
}
template<typename Scalar, typename MatrixType>
bool DataTransformer<Scalar, MatrixType>::copyGPUtoCPU(const GMatrix<Scalar> &from, MatrixType &to, cudaStream_t & stream) {
    bool dimension_check_result = check_dimension(from, to);
    if(dimension_check_result){
        Scalar * host_ptr  = to.data();
        copyGPUtoCPU(from.data(), from.rows(), from.cols(), host_ptr, stream);
    }
    return true;
}
template<typename Scalar, typename MatrixType>
bool DataTransformer<Scalar, MatrixType>::copyCPUtoGPU(const MatrixType &from, GMatrix<Scalar> &to, cudaStream_t & stream) {
    bool dimension_check_result = check_dimension(from, to);
    if(dimension_check_result){
        const Scalar * host_ptr = from.data();
        copyCPUtoGPU(host_ptr, from.rows(), from.cols(), to.data(), stream);
    }
    return true;
}


bool test_copy_from_cpu_and_back() {
    cudaStream_t stream;
    cudaStreamCreate(&stream);

    Eigen::MatrixXd host_data=Eigen::MatrixXd::Constant(10,10,2.0);

    Eigen::MatrixXd host_data_after_copy = Eigen::MatrixXd::Constant(10,10,0.2);
    GMatrix<double> dev_data(10,10);
    host_data.setConstant(1.0);
    cout << "host data before copy is " << endl << host_data <<  endl;

    DataTransformer<double, Eigen::MatrixXd>::copyCPUtoGPU(host_data, dev_data, stream);
    cudaStreamSynchronize(stream);
    DataTransformer<double, Eigen::MatrixXd>::copyGPUtoCPU(dev_data, host_data_after_copy, stream);
    cudaStreamSynchronize(stream);
    cout << "After Copying, host data is " << endl << host_data << endl;
    cudaStreamDestroy(stream);
    return host_data.isApprox(host_data_after_copy);

}