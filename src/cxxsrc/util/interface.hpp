#ifndef NSPCA_INTERFACE_CU
#define NSPCA_INTERFACE_CU

//*
//
// This file contains the interface of NSPCA solver routine
// And the interface of Monte Carlo
//
// */
#include <cstdlib>
#include <cassert>
#include <iostream>
#include "macro.hpp"
template<typename Derived>
class NSPCASolverInterface {
private:
    size_t N; //Number of Observations
    size_t P;
    size_t p;

    double lambda; //Shrinkage Parameter
    double scale; //scala parameter, equals to s in the manuscript
    double scale_sqaure;
public:
    NSPCASolverInterface(const size_t _N, const size_t _P, const size_t _p, const double _scale){
        assert(p<P&&"The reduction dimension has to be smaller than the original dimension");
        assert(_scale > 0 && "Scale factor has to be larger than 0");
        N=_N; P=_P;p=_p;
        scale = _scale;
        scale_sqaure = scale*scale;
    }
    Derived & derived(){
        return *static_cast<Derived*>(this);
    }
    const Derived & derived() const {
        return *static_cast<Derived*>(this);
    }

    size_t getN(){
        return N;
    }
    size_t getP(){
        return P;
    }
    size_t getp(){
        return p;
    }
    double getScale(){
        return scale;
    }
    void setScale(const double _scale){
        assert(_scale>0&&"Scale factor has to be larger than 0");
        scale = _scale;
    }
/*
 *
 * Interface for the driver routine
 *
 * */
    void init(){
        derived().init();
    }
//    void solve(const double _lambda_min, const double _lambda_max, const double increment, const af::array& data, const af::array& restriction){
//        assert(_lambda_min>=0);
//        assert(_lambda_max>_lambda_min);
//        assert(increment>0);
//        init();
//
//        const int step_needed = (int)((_lambda_max-_lambda_min)/increment);
//        double current_lambda=_lambda_min;
//        for (int i=0;i<step_needed;i++){
//            bool converged=false;
//            int count =0;
//            while(!converged){
//                count++;
//                copy_old_a();
//                solve_for_matrix_p(lambda, data, restriction);
//                solve_for_matrix_z(lambda, data, restriction);
//                solve_for_matrix_a(lambda, data, restriction);
//                converged=test_convergence(__TOLERATE, __MAX_ITER);
//            }
//        }
//    }
//    void solve_for_matrix_a(const double lambda, const af::array& data, const af::array& restriction){
//        derived().solve_for_matrix_a(lambda, data, restriction);
//    }
//    void solve_for_matrix_p(const double lambda, const af::array& data, const af::array& restriction){
//        derived().solve_for_matrix_a(lambda, data, restriction);
//    }
//    void solve_for_matrix_z(const double lambda, const af::array& data, const af::array& restriction){
//        derived().solve_for_matrix_z(lambda, data, restriction);
//    }

private:
    void copy_old_a(){
        std::cout << "To be implemented" << std::endl;
    }
    bool test_convergence(const double tolerance, const int max_iter){
        std::cout << "To be implemented " << std::endl;
        return true;
    }

};

#endif