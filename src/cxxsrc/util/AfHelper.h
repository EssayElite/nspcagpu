//
// Created by user on 9-11-16.
//

#ifndef NSPCA_AFHELPER_H
#define NSPCA_AFHELPER_H

#include <Eigen/Dense>
#include "arrayfire.h"
using namespace Eigen;

template<typename Scalar>
class AfHelper {
public:
    static void copyFromEigen(const Matrix<Scalar, Dynamic, Dynamic> & from, af::array & to);
    static void copyToEigen(Matrix<Scalar, Dynamic, Dynamic> & to, const af::array & from);
};

template class AfHelper<double>;
template class AfHelper<int>;

#endif //NSPCA_AFHELPER_H
