//
// Created by user on 6-11-16.
//

#ifndef NSPCA_INTERNAL_H
#define NSPCA_INTERNAL_H


template<typename lhs, typename rhs>
class select_type_impl_host{
    using type = double;
};

template<>
class select_type_impl_host<double, float>{
    using type = double;
};

template<>
class select_type_impl_host<double, int>{
    using type = double;
};

template<>
class select_type_impl_host<float, double>{
    using type = double;
};

template<>
class select_type_impl_host<int, float>{
    using type = float;
};

template<>
class select_type_impl_host<int, double>{
    using type = double;
};


template<>
class select_type_impl_host<float, int>{
    using type = float;
};


template<typename lhs, typename rhs>
using select_type_host = typename select_type_impl_host<lhs, rhs>::type;
#endif //NSPCA_INTERNAL_H
