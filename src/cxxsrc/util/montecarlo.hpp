#ifndef NSPCA_MONTECARLO_CPP
#define NSPCA_MONTECARLO_CPP

#include <cstdlib>
#include "random.h"
#include "../../fortransrc/fortranlib.h"
#include <Eigen/Dense>
template<typename MatrixDouble, typename MatrixInt>
class MonteCarlo {
public:

    MatrixDouble trueA;
    MatrixDouble trueP;
    MatrixDouble trueAP;
    MatrixDouble epsilon;

//    Eigen::MatrixXd temp_A; //This is used to hold data for the orthogonalization
    Random<double> random;

    const size_t N;
    const size_t P;
    const size_t p;
    MonteCarlo(const size_t _N, const size_t _P, const size_t _p):trueA(_N, _p), trueP(_p, _P), trueAP(_N, _P), epsilon(_N, _P), N(_N), P(_P), p(_p){}

    void generate(MatrixInt & data, const double std, const double upper, const double lower){
        assert(data.rows()==N);
        assert(data.cols()==P);
        assert(upper > lower);
        print_generating_info(std, upper, lower);
        generate_a_p_and_epsilon(std);
        orthonomalize();

        for (int i = 0; i < data.rows(); i++) {
            for (int j = 0; j < data.cols(); j++) {
                if (trueAP(i, j)+epsilon(i,j) > upper) {
                    data(i, j) = 1.0;
                } else if (trueAP(i, j)+epsilon(i,j) < lower) {
                    data(i, j) = -1.0;
                } else {
                    data(i, j) = 0.0;
                }
            }
        }
    }


    void print_generating_info(const double std, const double upper, const double lower) const {
        std::cout << " Generating from std = " << std << std::endl;
        std::cout << " upper = " << upper << std::endl;
        std::cout << " and lower = " << lower << std::endl;
    }

private:
    void orthonomalize(){
        int rows = trueA.rows();
        int cols = trueA.cols();
        double *  trueA_ptr= trueA.data();
        gramschmidt_(trueA_ptr, &rows, &cols);
    }
    void generate_a_p_and_epsilon(const double std){
        double * trueA_ptr = trueA.data();
        double * trueP_ptr = trueP.data();
        double * epsilon_ptr = epsilon.data();
        random.normal(trueA_ptr, N * p, 0, 1);
        random.normal(trueP_ptr, p * P, 0, 1);
        random.normal(epsilon_ptr, N*P, 0, std);
    }

};

template class MonteCarlo<Eigen::MatrixXd, Eigen::MatrixXi>;

#endif //NSPCA_MONTECARLO_H
