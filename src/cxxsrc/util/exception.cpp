//
// Created by user on 4-11-16.
//

#include <iostream>
#include "exception.h"

DimensionMisMatch::DimensionMisMatch(size_t lhs_row, size_t lhs_col, size_t rhs_row, size_t rhs_col, std::string lhs_name,
                                     std::string rhs_name)
        : lhs_row(lhs_row), lhs_col(lhs_col), rhs_row(rhs_row), rhs_col(rhs_col), lhs_name(lhs_name), rhs_name(rhs_name){}

size_t DimensionMisMatch::getLhs_row() const {
    return lhs_row;
}

size_t DimensionMisMatch::getLhs_col() const {
    return lhs_col;
}

size_t DimensionMisMatch::getRhs_row() const {
    return rhs_row;
}

size_t DimensionMisMatch::getRhs_col() const {
    return rhs_col;
}

void DimensionMisMatch::print_err() {
    print_err_type();
    print_dimension();
}

void DimensionMisMatch::print_dimension() {
    std::cout << lhs_name << " ";
    printf(" is of dimension (%lu, %lu)\n ", getLhs_row(), getLhs_col());
    std::cout << rhs_name << " ";
    printf(" is of dimension (%lu, %lu)\n", getRhs_row(), getRhs_col());
}

DimensionMisMatchMatMul::DimensionMisMatchMatMul(size_t lhs_row, size_t lhs_col, size_t rhs_row, size_t rhs_col, std::string lhs_name, std::string rhs_name)
        : DimensionMisMatch(lhs_row, lhs_col, rhs_row, rhs_col, lhs_name, rhs_name) {}

void DimensionMisMatchMatMul::print_err_type() {
    std::cout << "Dimension mismatch in matrix product" << std::endl;
}

DimensionMisMatchMatrixCopy::DimensionMisMatchMatrixCopy(size_t lhs_row, size_t lhs_col, size_t rhs_row, size_t rhs_col, std::string lhs_name, std::string rhs_name)
        : DimensionMisMatch(lhs_row, lhs_col, rhs_row, rhs_col, lhs_name, rhs_name) {}

void DimensionMisMatchMatrixCopy::print_err_type() {
    std::cout << "Dimension mismatch in copying matrix " << std::endl;
}
