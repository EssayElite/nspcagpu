//
// Created by user on 8-11-16.
//

#ifndef NSPCA_NSPCASOLVER_H
#define NSPCA_NSPCASOLVER_H
#include <Eigen/Dense>
using namespace Eigen;
#include <iostream>
#include <cstdlib>
#include "util/GMatrix.h"
#include "Threadpool/include/ThreadPoolMod/ThreadPool.h"
#include "cuda_runtime.h"
#include "magma.h"
#include "arrayfire.h"
#include "af/cuda.h"

#define __NUM_STREAMS 4
class NSPCAsolver {
private:
/*
 * Utilities
 * */
    JacobiSVD<MatrixXd> * svd;
    threadpool pool;
    cudaStream_t streams[__NUM_STREAMS];

    cudaStream_t magma_stream;
    magma_queue_t magma_queue;
//    cudaStream_t af_cuda_stream;

public:
    /*
     *
     * Data that are needed for storing the results
     *
     * */
    size_t N;
    size_t P;
    size_t p;

    MatrixXd matZ; //N times P
    MatrixXd matA; //N times P
    MatrixXd matP; //p times P

    GMatrix<double> devZ;
    GMatrix<double> devA;
    GMatrix<double> devP;

    double lambda;
    double scale;
    double scale_square;

    double lower_lambda;
    double upper_lambda;
    double steps;
    double step_size;
    /*
     *
     *
     * Internal data for workspaces
     *
     * */
    //p times N
    MatrixXd matU;
    MatrixXd matPZ;
    MatrixXd matT;

    GMatrix<double> devU;
//    GMatrix<double> devS;
    GMatrix<double> devVT;
    GMatrix<double> devPZT;
    GMatrix<double> devZTA;
    /*
     *
     * References to the data and restriction
     *
     * */
    GMatrix<int> devData; //N*P
    GMatrix<int> devRestriction; //p * P

    /*
     *
     * Work spaces
     *
     * */
    double * lwork;
    MatrixXd hostU;
    VectorXd hostS;
    MatrixXd hostVT; //
    MatrixXd hostPZT;

    int optimal_size;
    NSPCAsolver(const size_t _N, const size_t _P, const size_t _p, const double scale_, const int numThreads);
    void solve(const MatrixXi & data, const MatrixXi & restriction, const double lower_lambda, const double upper_lambda, const int steps);
    void initialize_solver(const MatrixXi & data, const MatrixXi & restriction, const double lower_lambda, const double upper_lambda, const int steps);
    ~NSPCAsolver();
    void solve_A();
    bool solve_A_test_blas_operation();
    bool solve_A_test_svd();
    void solve_P(int numblocksconstant, int numblocks);

private:

    void init_with_svd(const MatrixXi & data);

    void check_signs_with_restriction(const MatrixXi &restriction);
};


#define P_THREADS_SETTING_CONSTANT 4
#define P_THREADS 4

#endif //NSPCA_NSPCASOLVER_H
