//
// Created by user on 8-11-16.
//

#include "NSPCAsolver.h"
#include "util/DataTransformer.h"

using std::cout;
using std::endl;

#include "util/MatHelper.hpp"
#include "../cudasrc/GPUAlgorithm.h"
#include "../cxxsrc/util/MagmaWrapper.h"

NSPCAsolver::NSPCAsolver(const size_t _N, const size_t _P, const size_t _p, const double scale_, const int numThreads)
        : pool(numThreads), N(_N), P(_P), p(_p), devZ(N, P), devA(N, p), devP(p,P), devU(p, p),
          devVT(p, N), devPZT(p, N), devZTA(P, p), devData(N, P), devRestriction(p, P){

    /*
       * Init cuda streams for copying data
       * */
    for (int i =0;i < __NUM_STREAMS;i++){
        cudaStreamCreate(&streams[i]);
    }

    /*
     * Init magma
     * */
    magma_init();
    magma_int_t dev = 0;
    magma_queue_create_v2(dev, &magma_queue);
    magma_stream = magma_queue_get_cuda_stream(magma_queue);

//
//    magma_dgesvd 	( 	magma_vec_t  	jobu,
//            magma_vec_t  	jobvt,
//            magma_int_t  	m,
//            magma_int_t  	n,
//            double *  	A,
//            magma_int_t  	lda,
//            double *  	s,
//            double *  	U,
//            magma_int_t  	ldu,
//            double *  	VT,
//            magma_int_t  	ldvt,
//            double *  	work,
//            magma_int_t  	lwork,
//            magma_int_t *  	info
//    )

/*
 *  Initialize CPU data
 * */
    matZ = MatrixXd::Constant(N, P, 0.0); matA = MatrixXd::Constant(N, p, 0.0); matP = MatrixXd::Constant(p, P, 0.0);
    lambda = 0.0;
    scale = scale_;
    scale_square = scale * scale;
/*
 *  Initialize steps and lambda information
 *  Should be changed later
 * */
    lower_lambda=0;
    upper_lambda=0;
    steps=0;
    step_size=0;
/*
 * Allocate Work Space
 * */
//
    matU = MatrixXd::Constant(P, p,0.0);
    matPZ = MatrixXd::Constant(p, N, 0.0);
    matT = MatrixXd::Constant(N, P, 0.0);
    hostU = MatrixXd::Constant(p,p,0.0);
    hostS = VectorXd::Constant(p,0.0);
    hostVT = MatrixXd::Constant(p, N,0.0);
    hostPZT = MatrixXd::Constant(p, N, 0.0);

    lwork = (double*)malloc(sizeof(double));
    magma_int_t info;
    magma_dgesvd(MagmaAllVec, MagmaSomeVec, p, N, (double *)hostPZT.data(), p, (double*)hostS.data(), (double *)hostU.data(), p,
                 (double *)hostVT.data(), p, lwork, -1, &info);
    optimal_size = (int)lwork[0];
    lwork  = (double *)malloc(sizeof(double)*optimal_size);
    svd = new JacobiSVD<MatrixXd>();

}


NSPCAsolver::~NSPCAsolver(){
    free(lwork);
    delete svd;
    for (int i=0;i<__NUM_STREAMS;i++){
        cudaStreamSynchronize(streams[i]);
    }
    magma_queue_destroy( magma_queue );
    magma_finalize();
}


void NSPCAsolver::solve(const MatrixXi &data, const MatrixXi & restriction, const double _lower_lambda, const double _upper_lambda,
                        const int _steps) {
    initialize_solver(data, restriction, _lower_lambda,_upper_lambda,_steps);
    cout << "Needed to Finish solving methods " << endl;

}

void NSPCAsolver::initialize_solver(const MatrixXi &data, const MatrixXi & restriction, const double _lower_lambda, const double _upper_lambda,
                                    const int _steps){
    lower_lambda = _lower_lambda;
    upper_lambda = _upper_lambda;
    steps = _steps;

    step_size = (upper_lambda-lower_lambda)/steps;
    lambda= lower_lambda;

    assert(data.rows()==N&&data.cols()==P);
    assert(restriction.rows()==p&&restriction.cols()==P);

    DataTransformer<int, MatrixXi>::copyCPUtoGPU(data, devData, streams[0]);
//    cout << "f1 " << endl;
    DataTransformer<int, MatrixXi>::copyCPUtoGPU(restriction, devRestriction, streams[1]);
//    cout << "f2 " << endl;
    /*
     *  Using the standardized svd as first input
     * */

    init_with_svd(data);
//    cout << "f3 " << endl;
    DataTransformer<double, MatrixXd>::copyCPUtoGPU(matZ, devZ, streams[2]);
//    cout << "f4 " << endl;
    DataTransformer<double, MatrixXd>::copyCPUtoGPU(matA, devA, streams[3]);
//    cout << "f5"  << endl;
    /*
     * Check whether input is consistent with the restriction from the restrictions
     * */
    check_signs_with_restriction(restriction);
//    cout << "f6"  << endl;
    DataTransformer<double, MatrixXd>::copyCPUtoGPU(matP, devP, streams[0]);
//    cout << "f7"  << endl;
    for (int i =0;i<__NUM_STREAMS;i++){
        cudaStreamSynchronize(streams[i]);
    }
    cout << "###################Initialization Finished#########################" << endl;
}
void NSPCAsolver::check_signs_with_restriction(const MatrixXi &restriction) {
    for (int i=0; i < p; i++){
        for (int j =0; j < P; j++){
            double temp = matP(i,j);
            if (temp>0 && restriction(i, j) == -1){
                matP(i, j) = -temp;
            }
            else if (temp< 0 && restriction(i, j) == 1){
                matP(i, j) = -temp;
            }
            else if (restriction(i,j)==0){
                matP(i, j)=0;
            }
        }
    }
}

void NSPCAsolver::init_with_svd(const MatrixXi &data) {
    matZ = data.cast<double>();
    MatHelper::standardize(matZ);
    matZ = matZ*scale;
    svd->compute(matZ, ComputeThinU | ComputeThinV);
    matA = scale_square*svd->matrixU().block(0,0,N, p);
    MatrixXd temp =  svd->singularValues().asDiagonal();
    matP = temp.block(0,0,p,p)*svd->matrixV().transpose().block(0,0,p,P);
    matP = matP*scale;
//    cout << "Flags in init with svd " << endl;
}

void NSPCAsolver::solve_A() {
    /*
     * Use magma blas to calculate the matrix product
     * Does not work yet due to a bug in magma files
     * */

    magma_dgemm_q(MagmaNoTrans, MagmaTrans, p, N, P, 1.0, devP.data(), p, devZ.data(), N, 0.0, devPZT.data(), p,magma_queue);
    magma_queue_sync(magma_queue);

    DataTransformer<double, MatrixXd>::copyGPUtoCPU(devPZT, hostPZT, magma_stream);
    cudaStreamSynchronize(magma_stream);

//    cout << "Host data input is " << hostPZT << endl;
    MatrixXd temp(hostPZT.rows(), hostPZT.cols());


    magma_int_t info;
    magma_dgesvd(MagmaAllVec, MagmaSomeVec, p, N, (double *)hostPZT.data(), p, (double*)hostS.data(), (double *)hostU.data(), p,
                 (double *)hostVT.data(), p, lwork, optimal_size+100, &info);

    /*
     * Copy host results back to GPU and perform the matrix manipulation
     * */

    DataTransformer<double, MatrixXd>::copyCPUtoGPU(hostU, devU, magma_stream);
    DataTransformer<double, MatrixXd>::copyCPUtoGPU(hostVT, devVT, streams[0]);
//    devU.print(magma_stream);
//    devV.print(streams[0]);
    cudaStreamSynchronize(magma_stream);
    cudaStreamSynchronize(streams[0]);

    magma_dgemm_q(MagmaTrans, MagmaTrans, N, p, p, scale, devVT.data(), p , devU.data(), p, 0.0, devA.data(), N ,magma_queue);
    magma_queue_sync(magma_queue);

//    cudaStreamSynchronize(magma_stream);
//    devA.print(magma_stream);


//    af_PZT.unlock();
//    af::svd(af_U, af_S, af_VT, af_PZT);
//    cudaStreamSynchronize(af_cuda_stream);
//    double * devU = af_U.device<double>();
//    double * devVT = af_VT.device<double>();
//
//    magma_dgemm(MagmaTrans, MagmaNoTrans, p, P, N, scale, devU, N, devVT, N, 0.0, devA.data(), p);
//    magma_queue_sync(magma_queue);
//    af_U.unlock();
//    af_VT.unlock();

//      af_PZT  = af::matmulNT(devP, devZ);
//
//      af::svd(af_U, devS, af_VT, af_PZT);
//      af::print("U ", af_U);
//      af::print("Vt ", af_VT);
//      devA = af::matmulTN(af_U, af_VT);
//      devA = devA*scale;
//      devA.eval();
//      af::print("After updating, A is ", devA);


}

bool NSPCAsolver::solve_A_test_blas_operation() {
    /*
     * PZt is p*N
     * */
    cout << "Here " << endl;
    magma_dgemm_q(MagmaNoTrans, MagmaTrans, p, N, P, 1.0, devP.data(), p, devZ.data(), N, 0.0, devPZT.data(), p,magma_queue);
    magma_queue_sync(magma_queue);
    cout << "Results from GPU is " << endl;
    devPZT.print(magma_stream);

    cout << "Results from CPU is " << endl;
    MatrixXd cpu_mat_pzt = matP*matZ.transpose();
    cout << cpu_mat_pzt << endl;
    MatrixXd gpu_mat_temp = cpu_mat_pzt;
    DataTransformer<double, MatrixXd>::copyGPUtoCPU(devPZT, gpu_mat_temp, magma_stream);
    cudaStreamSynchronize(magma_stream);
    double diff = (cpu_mat_pzt-gpu_mat_temp).norm();
    if (diff > 0.1){
        return false;
    }
    else {
        return true;
    }

//
//    af_PZT  = af::matmulNT(devP, devZ);
//    af::print("The matrix product in the gpu is ", af_PZT);
//    MatrixXd cpu_matPZT= matP*matZ.transpose();
//    cout << "The matrix product from cpu is " << endl << cpu_matPZT << endl;
//    MatrixXd gpu_matPZT = MatrixXd::Constant(cpu_matPZT.rows(), cpu_matPZT.cols(), 0.0);
//    af_PZT.eval();
//    AfHelper<double>::copyToEigen(gpu_matPZT, af_PZT);
//    cout << "After copying the matrix is " << endl << gpu_matPZT << endl;
//    double diff =(cpu_matPZT-gpu_matPZT).norm();
//    if (diff > 0.1){
//        return false;
//    }
//    else {
//        return true;
//    }
//    cout << "Testing the first step in blas operation,  namely PZT = P*Z^T" << endl;
//    double * devPtrPZT = af_PZT.device<double>();
//    cout << "P is " << endl << matP << endl;
//    cout << "Z is " << endl << matZ << endl;
//    MatrixXd cpu_matPZT= matP*matZ.transpose();
//    cout << "The matrix product from cpu is " << endl << cpu_matPZT << endl;
//    magma_dgemm_q(MagmaNoTrans, MagmaTrans, p, N, P, 1.0, devP.data(), p, devZ.data(), N, 0.0, devPtrPZT, p, magma_queue);
//    cudaStreamSynchronize(magma_stream);
//    MatrixXd gpu_result = MatrixXd::Constant(p, N, 0.1);
//    double * gpu_result_ptr = (double *)gpu_result.data();
//    DataTransformer<double, MatrixXd>::copyGPUtoCPU(devPtrPZT, p, N, gpu_result_ptr, magma_stream);
//
//    cout << "GPU result is " << endl;
//    cout << gpu_result << endl;
//    af_PZT.unlock();
//    cudaDeviceSynchronize();
}

bool NSPCAsolver::solve_A_test_svd() {
    magma_dgemm_q(MagmaNoTrans, MagmaTrans, p, N, P, 1.0, devP.data(), p, devZ.data(), N, 0.0, devPZT.data(), p,magma_queue);
    magma_queue_sync(magma_queue);

    DataTransformer<double, MatrixXd>::copyGPUtoCPU(devPZT, hostPZT, magma_stream);
    cudaStreamSynchronize(magma_stream);

    cout << "Host data input is " << hostPZT << endl;
    MatrixXd temp(hostPZT.rows(), hostPZT.cols());

    for (int i=0;i<temp.rows();i++){
        for (int j=0;j<temp.cols();j++){
            temp(i,j)=hostPZT(i,j);
        }
    }

    magma_int_t info;
    magma_dgesvd(MagmaAllVec, MagmaSomeVec, p, N, (double *)hostPZT.data(), p, (double*)hostS.data(), (double *)hostU.data(), p,
                 (double *)hostVT.data(), p, lwork, optimal_size+100, &info);
    MatrixXd hostS_diag= hostS.asDiagonal();
    cout << "host S is " << endl <<hostS_diag << endl;
    cout << "Host V is " << endl << hostVT << endl;
    cout << "host U is " << endl << hostU << endl;

    cout << "Recovered result is " << hostU*hostS_diag*hostVT << endl;
//    MatrixXd tmp = scale*hostVT.transpose()*hostU.transpose();
//    cout << "host results for solving a  is " << endl << tmp << endl;
    double diff = (temp-hostU*hostS_diag*hostVT).norm();
    if (diff > 0.1){
        return false;
    }
    else {
        return true;
    }
}

void NSPCAsolver::solve_P(int numBlocks_for_constant, int numblocks) {
    /*
     *
     * Setting P to 0's as initialization
     *
     * */

    const int size=devP.rows()*devP.cols();
    GPUAlgorithm<double, P_THREADS_SETTING_CONSTANT, 4>::setConstant(devP.data(), size, 0.0, numBlocks_for_constant, streams[0]);
    MagmaWrapper::matmulTN(devZTA, devZ, devA, 1.0, 0.0, magma_queue);
    
}





